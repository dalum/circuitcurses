from itertools import chain

import curses

STACK = []

MACROENUM = {0: "-", 1: "Osc1Semi", 2: "Osc1Cents", 3: "Osc1Wave", 4: "Osc1Idx", 5: "Osc1VSync"}
MODSRCENUM = {0: "-", 1: "Osc1Semi", 2: "Osc1Cents", 3: "Osc1Wave", 4: "Osc1Idx", 5: "Osc1VSync"}

class Widget:
    def __init__(self, name, value, vmin=0, vmax=127, venum={}, width=15, majormult=10):
        self.__dict__.update(locals())

    def update(self):
        self.value = max(self.vmin, min(self.vmax, self.value))

    def incminor(self, val):
        self.value += val

    def incmajor(self, val):
        self.value += self.majormult*val

    def draw(self, stdscr, y, x, hover=False, sel=False):
        dim= curses.A_DIM
        normal = curses.A_NORMAL
        highlight = curses.A_REVERSE
        underline = curses.A_UNDERLINE
        value = str(self.venum.get(self.value, self.value))
        stdscr.addstr(y, x, self.name[0:self.width], highlight if hover and not sel else underline if hover else normal)
        stdscr.addstr(y + 1, x + 1, value[0:self.width-1], highlight if hover and sel else dim)

    def getwidth(self):
        return self.width

class Table:
    def __init__(self, *rows, locy=0, locx=0, sel=False):
        self.__dict__.update(locals())

    def keypress(self, c):
        if c == 32:
            self.sel = not self.sel

        if self.sel:
            if c == curses.KEY_RIGHT:
                self.rows[self.locy][self.locx].incminor(1)
            elif c == curses.KEY_LEFT:
                self.rows[self.locy][self.locx].incminor(-1)
            if c == curses.KEY_UP:
                self.rows[self.locy][self.locx].incmajor(1)
            elif c == curses.KEY_DOWN:
                self.rows[self.locy][self.locx].incmajor(-1)
        else:
            if c == curses.KEY_RIGHT:
                self.locx += 1
            elif c == curses.KEY_LEFT:
                self.locx -= 1
            elif c == curses.KEY_UP:
                self.locy -= 1
            elif c == curses.KEY_DOWN:
                self.locy += 1

    def update(self):
        self.locy = max(0, min(self.locy, len(self.rows) - 1))
        self.locx = max(0, min(self.locx, len(self.rows[self.locy]) - 1))

        for row in self.rows:
            for widget in row:
                widget.update()

    def draw(self, stdscr, y, x):
        for (j, row) in enumerate(self.rows):
            for (i, widget) in enumerate(row):
                if (self.locy, self.locx) == (j, i):
                    widget.draw(stdscr, y, x, hover=True, sel=self.sel)
                else:
                    widget.draw(stdscr, y, x)
                x += widget.getwidth() + 1
            x = 0
            y += 2

    def getheight(self):
        return 2*len(self.rows)

class MenuItem:
    def __init__(self, name, target, width=15):
        self.__dict__.update(locals())

    def update(self):
        pass

    def draw(self, stdscr, y, x, hover=False):
        normal = curses.A_NORMAL
        highlight = curses.A_REVERSE
        stdscr.addstr(y, x, self.name[0:self.width], highlight if hover else normal)

    def getwidth(self):
        return self.width

class Menu:
    def __init__(self, *rows, locy=0, locx=0):
        self.__dict__.update(locals())

    def update(self):
        self.locy = max(0, min(self.locy, len(self.rows) - 1))
        self.locx = max(0, min(self.locx, len(self.rows[self.locy]) - 1))

        for row in self.rows:
            for item in row:
                item.update()

    def keypress(self, c):
        if c == 32:
            STACK.append(self.rows[self.locy][self.locx].target)
        elif c == curses.KEY_RIGHT:
            self.locx += 1
        elif c == curses.KEY_LEFT:
            self.locx -= 1
        elif c == curses.KEY_UP:
            self.locy -= 1
        elif c == curses.KEY_DOWN:
            self.locy += 1

    def draw(self, stdscr, y, x):
        for (j, row) in enumerate(self.rows):
            for (i, item) in enumerate(row):
                if (self.locy, self.locx) == (j, i):
                    item.draw(stdscr, y, x, hover=True)
                else:
                    item.draw(stdscr, y, x)
                x += item.getwidth() + 1
            x = 0
            y += 2

    def getheight(self):
        return 2*len(self.rows)

def main(stdscr):
    miny, minx = stdscr.getbegyx()
    maxy, maxx = stdscr.getmaxyx()

    pad = curses.newpad(1000, min(maxx, 64))

    curses.halfdelay(1)
    curses.curs_set(0)

    macrotable = Table(*chain(*[
        (
            [Widget(f"Macro {j} Dest {i}", 0, vmax=4, venum=MACROENUM, majormult=1)
            for i in "ABCD"],
            [Widget(f"Macro {j} Depth {i}", 0, vmin=-64, vmax=63) for i in "ABCD"]
        )
        for j in range(1, 9)
    ]))

    modtable = Table(*[
        [
         Widget(f"Mod {j} Src 1", 0, vmax=4, venum=MODSRCENUM, majormult=1),
         Widget(f"Mod {j} Src 2", 0, vmax=4, venum=MODSRCENUM, majormult=1),
         Widget(f"Mod {j} Depth", 0, vmin=-64, vmax=63)
        ]
        for j in range(1, 21)]
    )

    osctable = Table(
        [Widget("Cutoff", 0), Widget("Resonance", 127)],
        [Widget("Rate", 64), Widget("Sync", 0, vmax=1, venum={0: "Off", 1: "On"})]
    )

    lfotable = Table(
        [Widget("Rate", 67), Widget("Sync", 0, vmax=1, venum={0: "Off", 1: "On"}),
         Widget("Slew", 0), Widget("Phase", 0)],
        [Widget("Delay", 0), Widget("Sync", 0, vmax=1, venum={0: "Off", 1: "On"}),
         Widget("DTrigger", 0, vmax=1, venum={0: "Single", 1: "Multi"}),
         Widget("Delay Fade", 0, majormult=1, vmax=3,
                venum={0: "Fade in", 1: "Gate in",
                       2: "Fade out", 3: "Gate out"})]
    )

    mainmenu = Menu([MenuItem("Macros", macrotable), MenuItem("Mod Matrix", modtable), MenuItem("LFOs", lfotable)],
                    [MenuItem("Oscillators", osctable)])
    STACK.append(mainmenu)

    while True:
        c = stdscr.getch()
        if c == ord('q'):
            if len(STACK) > 1:
                STACK.pop()

        if c == ord('x'):
            break

        STACK[-1].keypress(c)
        STACK[-1].update()

        pad.clear()
        STACK[-1].draw(pad, 0, 0)
        pady = int(min(STACK[-1].getheight() - maxy, max(0, 2*STACK[-1].locy - maxy / 2)))
        pad.refresh(pady, 0, 0, 0, maxy - 1, maxx - 1)

if __name__ == "__main__":
    curses.wrapper(main)
